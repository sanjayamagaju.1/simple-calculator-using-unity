using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Calculator : MonoBehaviour
{
    public TextMeshProUGUI inputText;
    private float firstInput;
    private float secondInput;
    private string operation;
    private float result;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // To enter number
    public void enterNumber(int num) 
    {
        Debug.Log(num);
        inputText.text = num.ToString();
        if(firstInput == 0) {
            firstInput = num;
        }
        else {
            secondInput = num;
        }
    }

    // For operations
    public void enterOperation(string val) 
    {
        Debug.Log(val);
        operation = val;
    }

    // enter equal
    public void enterEqual(string equal) 
    {
        Debug.Log(equal);
        if (firstInput != 0 && secondInput != 0 && operation != null)
        {
            switch (operation)
            {
                case "+":
                    result = firstInput + secondInput;
                    break;
                case "-":
                    result = firstInput - secondInput;
                    break;
                case "*":
                    result = firstInput * secondInput;
                    break;
                case "/":
                    result = firstInput / secondInput;
                    break;
            }
            inputText.SetText(result.ToString());
        }
    }

    // enter point
    public void enterPoint(string point) 
    {
        Debug.Log(point);
    }

    // reset value 
    // note: enter reset button C evertime after one successful operation.
    public void enterReset(string C) 
    {
        // Debug.Log(C);
        firstInput = 0;
        secondInput = 0;
        inputText.text = "";
    }
}
